desc "Updates db data based on Facebook data"

task :debug do
  # Do nothing, for allowing debug for facebook and google modes
end

task :facebook => :environment do
  fb_driver = FbSync.new
  fb_driver.update(ARGV[1] != "debug")
end

task :google => :environment do
  g_driver = GoogleSync.new
  g_driver.update(ARGV[1] != "debug")
end

task :check_names => :environment do
  g_driver = GoogleSync.new
  mismatched_names = g_driver.detect_mismatched_names()
  mismatched_names.each do |team, posts|
    puts "Mismatched names for team: #{team}"
    posts.each do |post|
      puts "    #{post.author}"
    end
  end
end
