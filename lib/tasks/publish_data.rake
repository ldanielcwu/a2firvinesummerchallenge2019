desc "Updates db data based on Facebook data"

def get_worksheet(session, ss_key, ws_title)
  return session.spreadsheet_by_key(ss_key).worksheet_by_title(ws_title)
end

task :publish => :environment do
  session = GoogleDrive::Session.from_config("config.json")
  ws_dest = get_worksheet(session, "1FmmUm7prs5I9bgtLy8XreyuA7Ljd2zC1fh8iMefpE-4", "Dashboard")
  ws_src = get_worksheet(session, "1ZgIUm_dsUgFoUmOuzV-b7En401MqwPzZ16MjQdjbv0Y", "Dashboard")

  num_rows = ws_src.num_rows
  row_num = 1
  counting = false
  counting_index = 0
  while row_num <= num_rows
    # Figure out whether or not to edit based on this row
    row_title = ws_src[row_num, 2].strip
    if row_title == "Lavender Lions"
      counting = true
      counting_index += 1
    end
    counting = false if row_title == "Total:" or row_title == ""

    if counting
      col_index = 3
      max_col = (counting_index < 3) ? 10 : 41
      while col_index <= max_col
        ws_dest[row_num, col_index] = ws_src[row_num, col_index]
        col_index += 1
      end
    end

    row_num += 1
  end
  ws_dest.save
end
