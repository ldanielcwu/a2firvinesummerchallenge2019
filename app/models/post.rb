class Post < ApplicationRecord
  # Make sure that lists can be serialized as strings in the Rails DB
  serialize :challenges

  ##
  # Populate data fields for a new User object when it is initialized
  def init_actions(new_post)
    new_post.challenges ||= []
    challenges = new_post.challenges
    new_post.challenges = YAML.load(challenges) if challenges.class == String
  end

end
