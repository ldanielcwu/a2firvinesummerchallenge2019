/**
 * Fades the currently displayed flash message after some time
 */
$(document).on('turbolinks:load', function() {
    var closeNotice = setInterval(function() {
        if ($('.fadeIn').length == 0) {
            clearInterval(closeNotice);
        }
        $('.notice').removeClass('fadeIn').addClass('fadeOut');
    }, 6000);
});
