function loadModals() {
    var elems = document.querySelectorAll('.modal');
    var modal = M.Modal.init(elems, {})[0];

    $(".modal-trigger").on("click", function() {
        var row = $(this).parent();
        
        var post_id = parseInt(row.find(".post_id").html());
        var author = row.find(".post_author").html();
        var author_profile_link = row.find(".post_author_profile").attr("src");
        var challenges = row.find(".post_challenges").html();
        var message = row.find(".post_message").html();
        var team = row.find(".post_team").html();
        var timestamp = row.find(".post_timestamp").html();
        var is_processed = row.find(".post_is_processed").html() == "true";

        $(".modal").find(".edit_post_id").val(post_id);
        $(".modal").find(".edit_post_author").html(author);
        $(".modal").find(".edit_post_author_profile").attr("src", author_profile_link);
        $(".modal").find(".edit_post_challenges").val(challenges);
        $(".modal").find(".edit_post_is_processed").prop("checked", is_processed);
        $(".modal").find(".edit_post_message").html(message);
        $(".modal").find(".edit_post_team").val(team);
        $(".modal").find(".edit_post_timestamp").html(timestamp);
        if (team != "")
            $(".modal").find(".edit_post_team_label").addClass("active");
        if (challenges != "")
            $(".modal").find(".edit_post_challenges_label").addClass("active");
        
        modal.open();
    });

    $(".modal-backdrop").on("click", function() {
        modal.close();
    });
}

$(document).on('turbolinks:load', loadModals);
