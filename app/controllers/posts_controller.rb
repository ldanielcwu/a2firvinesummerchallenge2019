class PostsController < ApplicationController
  include ApplicationHelper

  before_filter :handle_show_processed

  def view
    if @show_processed
      @posts = Post.where(is_processed: true)
    else
      @posts = Post.where(is_processed: false)
    end
    @posts = @posts.sort_by { |post| date_to_int(post.timestamp) }.reverse
  end

  def invalid_names
    @posts = []
    GoogleSync.new.detect_mismatched_names.each do |team, team_posts|
      team_posts.each do |post|
        @posts << post
      end
    end
  end

  def edit
    @post = Post.find(params["post_id"])
    @post.challenges = params["challenges"].split(", ")
    @post.team = params["team"]
    @post.is_processed = params["is_processed"].to_i
    @post.save!
    redirect_back(fallback_location: view_posts_path)
 end

  def refresh
    posts = FbSync.new.update()

    num_posts = posts.length
    if num_posts == 0
      flash[:error] = "Unable to download posts - please contact Daniel Taipei"
    else
      flash[:notice] = "Downloaded #{num_posts} posts!"
    end
    redirect_to view_posts_path
  end

  private

  def handle_show_processed()
    @show_processed = params.has_key?(:show_processed) && (params[:show_processed] == "true")
  end
end
