require 'google_drive'
Google::Apis.logger = Logger.new("google_output.txt")

class GoogleSync
  include ApplicationHelper

  def update(loop_forever = true)
    team_data = get_team_data()
    session = GoogleDrive::Session.from_config("config.json")
    while true
      posts = get_posts()
      report = Hash.new
      team_data.each do |team, spreadsheet_key|
        next if not posts.has_key?(team) # Team has no posts, don't waste time and API accesses opening docs
        puts "Updating spreadsheet for: #{team}"
        ss = session.spreadsheet_by_key(spreadsheet_key)
        report[team] = { achievements: 0, recurring: 0 }

        # Handle Achievements
        synced_posts = []
        ws = ss.worksheet_by_title("Achievements")
        name_cols = get_achievement_name_cols(ws)
        challenge_rows = get_achievement_challenge_rows(ws)
        posts[team][:achievements].each do |post|
          col = name_cols[post.author.downcase]
          next if col == nil

          had_challenge = false
          post.challenges.each do |challenge|
            next if not challenge_rows.has_key?(challenge)
            had_challenge = true
            row = challenge_rows[challenge]
            ws[row, col] = "TRUE"
            ws[row, col + 1] = post.timestamp.split(", ")[0]
            report[team][:achievements] += 1
          end
          if had_challenge
            synced_posts << post
          end
        end
        ws.save
        synced_posts.each do |post|
          post.is_published = true
          post.save!
        end

        # Handle Recurring Achievements
        synced_posts = []
        num_recurring = 0
        ws = ss.worksheet_by_title("Recurring Achievements")
        recurring_data = organize_recurring_data(posts[team][:recurring])
        cur_row = get_recurring_start_row(ws)
        recurring_data.each do |date, r_data|
          r_data.each do |author, r_a_data|
            next if !(r_a_data[:dt] || r_a_data[:exercise] || r_a_data[:perimeter_72])
            ws[cur_row, 2] = date
            ws[cur_row, 3] = author
            ws[cur_row, 4] = r_a_data[:exercise] ? "TRUE" : "FALSE"
            ws[cur_row, 5] = r_a_data[:dt] ? "TRUE" : "FALSE"
            ws[cur_row, 6] = r_a_data[:perimeter_72] ? "TRUE" : "FALSE"
            report[team][:recurring] += 1
            synced_posts << r_a_data[:post]
            cur_row += 1
          end
        end
        ws.save
        synced_posts.each do |post|
          post.is_published = true
          post.save!
        end
      end

      report.each do |team, team_data|
        team_data.each do |category, count|
          if count > 0
            puts "New Entries for #{team}: #{category} - #{count}"
          end
        end
      end
      break if not loop_forever
    end
  end

  def detect_mismatched_names()
    posts = get_posts()
    team_data = get_team_data()

    mismatched_names = Hash.new
    session = GoogleDrive::Session.from_config("config.json")
    team_data.each do |team, spreadsheet_key|
      next if not posts.has_key?(team) # Team has no posts, don't waste time opening docs

      # Only need to scan achievements sheet for this
      ss = session.spreadsheet_by_key(spreadsheet_key)
      ws = ss.worksheet_by_title("Achievements")
      name_cols = get_achievement_name_cols(ws, false)

      # If no column for a name, we have a mismatched name
      posts[team][:achievements].each do |post|
        col = name_cols[post.author]
        next if col != nil

        mismatched_names[post.team] = [] if not mismatched_names.has_key?(post.team)
        mismatched_names[post.team] << post
      end
    end

    return mismatched_names
  end

  private

  def get_posts()
    organized_posts = Hash.new
    Post.where(is_published: false).each do |post|
      next if not post.is_processed
      if (post.team == "") || (post.challenges == "")
        post.is_published = true # We're marking admin/staff/DQ'd posts as published so we don't try to republish them
        post.save!
        next
      end

      organized_posts[post.team] = { achievements: [], recurring: [] } if not organized_posts.has_key?(post.team)

      if (post.challenges.include?("DT") or post.challenges.include?("Exercise") or post.challenges.include?("Perimeter 72"))
        organized_posts[post.team][:recurring] << post
      else
        organized_posts[post.team][:achievements] << post
      end
    end
    return organized_posts
  end

  def get_achievement_name_cols(ws, downcase_names=true)
    name_cols = Hash.new
    col_index = 1
    start_counting = false
    num_cols = ws.num_cols
    while col_index <= num_cols
      if not start_counting
        start_counting = true if (ws[2, col_index] =~ /^Points:/)
      end
      if start_counting
        name = ws[1, col_index].strip
        name = name.downcase if downcase_names
        name_cols[name] = col_index if name != ""
      end
      col_index += 1
    end
    return name_cols
  end

  def get_achievement_challenge_rows(ws)
    challenge_rows = Hash.new
    row_index = 1
    while row_index <= ws.num_rows
      cell_contents = ws[row_index, 1]
      challenge_rows[cell_contents.strip] = row_index if (cell_contents =~ /\d/) && (cell_contents !~ /points/i)
      row_index += 1
    end
    return challenge_rows
  end

  def get_recurring_start_row(ws)
    row_index = 1
    start_counting = false
    num_rows = ws.num_rows
    while row_index <= num_rows
      cell_contents = ws[row_index, 2].strip
      if not start_counting
        start_counting = true if cell_contents == "Date"
      else
        return row_index if cell_contents.strip == ""
      end
      row_index += 1
    end
  end

  def organize_recurring_data(posts)
    data = Hash.new
    posts.each do |post|
      t = post.timestamp.split(", ")[0]
      a = post.author

      data[t] = Hash.new if not data.has_key?(t)
      data[t][a] = Hash.new if not data[t].has_key?(a)

      data[t][a][:post] = post
      data[t][a][:dt] = post.challenges.include?("DT")
      data[t][a][:exercise] = post.challenges.include?("Exercise")
      data[t][a][:perimeter_72] = post.challenges.include?("Perimeter 72")
    end
    return data
  end

  def get_team_data
    data = Hash.new
    data["Lavender Lions"] = "1izIri2_pIPHs2HCO3q5ajbJKd4YkNQBHzvpQ1G1t1FQ"
    data["Platinum Pegasus"] = "18LbRenaE8f0KUTHuZskz8EqPoHqlxq8r83Db9ZWYqzc"
    data["Red Robins"] = "14aMwKvUACLHOiKctxi7kEGXTY-7MK8AawEG3C-OzCOo"
    data["Yellow Yaks"] = "1AKtM7_tJC4rt2cRdQ1nfP24I27u1TuFjy-hfrCbzU8s"
    return data
  end

end
