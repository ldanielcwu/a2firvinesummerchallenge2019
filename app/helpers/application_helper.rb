module ApplicationHelper

  def compare_dates(date1, date2)
    return 1 if date1 == ""

    date1_array = date1.split("/")
    month1 = date1_array[0].to_i
    day1 = date1_array[1].to_i
    year1 = date1_array[2].to_i
    year1 += 2000 if year1 < 1000


    date2_array = date2.split("/")
    month2 = date2_array[0].to_i
    day2 = date2_array[1].to_i
    year2 = date2_array[2].to_i
    year2 += 2000 if year2 < 1000

    d1 = Date.new(year1, month1, day1)
    d2 = Date.new(year2, month2, day2)
    date_difference_buffer = 0
    if (d2 - d1) > date_difference_buffer
      return 1
    elsif (d1 - d2) > date_difference_buffer
      return -1
    end
    return 0
  end

  def date_to_int(date)
    date_array = date.split("/")
    month = date_array[0].to_i
    day = date_array[1].to_i
    year = date_array[2].to_i
    year += 2000 if year < 1000

    res = year * 10000
    res += month * 100
    res += day
    return res
  end

end
