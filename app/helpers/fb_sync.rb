require 'selenium-webdriver'
require 'yaml'

FACEBOOK_USERNAME = "FACEBOOK_USERNAME"
FACEBOOK_PASSWORD = "FACEBOOK_PASSWORD"

class FbSync
  include ApplicationHelper

  ####################################################################
  # Main Driver Code
  ####################################################################
  def update(loop_forever=true)
    begin
      driver = load_driver()
      driver.get("https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F1935183619921249%2F")
      login(driver)

      while true
        attempts = 0
        begin
          min_timestamp = Post.all.sort_by { |post| date_to_int(post.timestamp) }.last.timestamp
          min_timestamp = min_timestamp == nil ? "01/01/00" : min_timestamp.split(", ")[0]

          post_data = []
          post_data += get_posts(driver, "https://www.facebook.com/groups/1935183619921249/", min_timestamp, true) # Achievements
          post_data += get_posts(driver, "https://www.facebook.com/events/368447567139262/", min_timestamp) # Perimeter 72
          post_data += get_posts(driver, "https://www.facebook.com/events/572569813268625/", min_timestamp) # DT
          post_data += get_posts(driver, "https://www.facebook.com/events/368441233812733/", min_timestamp) # Exercise
          post_data += get_posts(driver, "https://www.facebook.com/events/462202467897217/", min_timestamp) # Flash Challenge - Perimeter

          # Process posts
          posts = []
          all_team_data = team_data()
          post_data.each do |data|

            # Upload content
            post = Post.where(timestamp: data["timestamp"], author: data["author"]).first_or_initialize
            post.message = data["message"]
            post.profile_link = data["profile_link"]

            challenges = []
            data["hashtags"].each do |hashtag|
              # Parse hashtag for team name
              all_team_data.each do |team_data|
                team_data[:aliases].each do |alias_name|
                  if hashtag == "##{alias_name}"
                    post.team = team_data[:name]
                  end
                end
              end

              # Parse hashtag for challenge number / name
              if not post.challenges.include?("DQ")
                if hashtag =~ /^#challenge(\d+)$/i
                  challenges << "#{$1}"
                elsif hashtag =~ /^#challengef(\d+)$/i
                  challenges << "F#{$1}"
                elsif hashtag =~ /^#dtchallenge$/i
                  challenges << "DT"
                elsif hashtag =~ /^#healthchallenge$/i
                  challenges << "Exercise"
                elsif hashtag =~ /^#exercise$/i
                  challenges << "Exercise"
                elsif hashtag =~ /^#perimeter\d+$/i
                  challenges << "Perimeter 72"
                end
              end
            end
            post.challenges = challenges if challenges.length > 0

            # Save post
            post.is_processed = true if (post.team != "") && (post.challenges != "")
            post.save! if post.changed?
            posts << post
          end

          puts "Downloaded #{posts.length} posts!"
          break if not loop_forever
        rescue Exception => e
          attempts += 1
          if attempts >= 3
            raise e
          else
            retry
          end
        end
      end
    rescue Net::ReadTimeout => e
      retry
    end
  end

  private

  ####################################################################
  # Selenium helper method
  ####################################################################
  def load_driver()
    options = Selenium::WebDriver::Chrome::Options.new(args: ["headless", "--disable-notifications", "--disable-dev-shm-usage"])
    return Selenium::WebDriver.for(:chrome, options: options)
  end

  ####################################################################
  # Helper methods for interacting with a page
  ####################################################################
  def click(driver, selection_field, selection_name)
    checkpoint_button = driver.find_element(selection_field, selection_name)
    checkpoint_button.click
  end

  def fill_field(driver, id, value)
    element = driver.find_element(id: id)
    element.send_keys(value)
    return element
  end

  ####################################################################
  # Facebook-interaction helper methods
  ####################################################################
  def handle_security(driver)
    begin
      click(driver, :id, "checkpointSubmitButton")
      click(driver, :class, "_55sh")
      sleep 1
      click(driver, :id, "checkpointSubmitButton")
      click(driver, :class, "_55sh")
      sleep 1
      click(driver, :id, "checkpointSubmitButton")
    rescue
      # We aren't in a security checkpoint - proceed normally
    end
    sleep 2
  end

  def login(driver)
    element = fill_field(driver, "email", FACEBOOK_USERNAME)
    element = fill_field(driver, "pass", FACEBOOK_PASSWORD)
    element.submit

    handle_security(driver) # if we hit a security checkpoint
  end

  def scroll_to_bottom(driver)
    prev_height = driver.execute_script("return document.documentElement.scrollHeight")
    driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight)")
    new_height = driver.execute_script("return document.documentElement.scrollHeight")
    sleep 4
    return prev_height == new_height
  end

  def get_posts_helper(driver, min_timestamp, is_group)
    posts_data = Hash.new
    while true
      found_old_post = false
      driver.find_element(:id, (is_group ? "pagelet_group_mall" : "pagelet_event_mall" )).find_elements(:class, "userContentWrapper").each do |post|
        data = Hash.new

        # Get author, timestamp
        author_node = post.find_element(:class, "lfloat")
        data["author"] = author_node.attribute("title")
        data["profile_link"] = author_node.find_element(:tag_name, "img").attribute("src")
        data["timestamp"] = post.find_element(:tag_name, "abbr").attribute("title")
        data["message"] = ""
        data["hashtags"] = []
        user_content = post.find_element(:class, "userContent")
        data["message"] = user_content.attribute("textContent")
        if compare_dates(data["timestamp"].split(", ")[0], min_timestamp) > 0
          found_old_post = true
          break
        end

        # Filter out posts before we create them
        next if data["message"] == "" # Ignore blank messages
        next if data["author"] =~ /Gracepoint/
        next if data["author"] =~ /Jermaine/

        # Save data
        data["hashtags"] = data["message"].scan(/#[^ ,:\n\.\;\?#\^\&\(\)\-=\+]+/).map { |hashtag| hashtag.downcase }

        posts_data["#{data["timestamp"]}_#{data["author"]}"] = data
      end

      break if found_old_post
      was_at_bottom = scroll_to_bottom(driver)
      break if was_at_bottom
    end

    return posts_data.values
  end

  def get_posts(driver, link, min_timestamp, is_group=false)
    driver.get(link)
    return get_posts_helper(driver, min_timestamp, is_group)
  end

  ####################################################################
  # Challenge Data
  ####################################################################
  def team_data()
    data = []
    data << { aliases: ["lavenderlion", "lavenderlions"], name: "Lavender Lions" }
    data << { aliases: ["platinumpegasus", "platinumpegasuses", "platinumpegasi"], name: "Platinum Pegasus" }
    data << { aliases: ["redrobin", "redrobins"], name: "Red Robins" }
    data << { aliases: ["yellowyak", "yellowyaks"], name: "Yellow Yaks" }
    return data
  end

end
