Rails.application.routes.draw do

  # Dashboard routes
  resource :posts, only: [] do
    get  :view
    get  :invalid_names
    get  :refresh
    post :edit
  end
  root to: "posts#view"

  # Session routes
  resources :sessions , only: [:create, :destroy, :login]
  get 'auth/:provider/callback',       to: 'sessions#create'
  get 'auth/failure',                  to: redirect('/')
  get 'signout',                       to: 'sessions#destroy', as: 'signout'
  get 'signin',                        to: 'sessions#login',   as: 'signin'
end
