OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, '1016750862259-fs7mq4d6g3o29ca7u26dg5dj8oi5931a.apps.googleusercontent.com', '-34dMVTiyYP5ZLKEB3ROwuTK', {
    client_options: {
      ssl: {
        ca_file: Rails.root.join("cacert.pem").to_s
      }
    },
    scope: "email, profile, openid"
  }
end
