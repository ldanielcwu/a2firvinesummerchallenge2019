require 'selenium-webdriver'

FACEBOOK_USERNAME = "FACEBOOK_USERNAME"
FACEBOOK_PASSWORD = "FACEBOOK_PASSWORD"

def load_driver()
  options = Selenium::WebDriver::Chrome::Options.new(args: ["headless", "--disable-notifications", "--disable-dev-shm-usage"])
  return Selenium::WebDriver.for(:chrome, options: options)
end

def click(driver, selection_field, selection_name)
  checkpoint_button = driver.find_element(selection_field, selection_name)
  checkpoint_button.click
end

def fill_field(driver, id, value)
  element = driver.find_element(id: id)
  element.send_keys(value)
  return element
end

def login(driver)
  element = fill_field(driver, "email", FACEBOOK_USERNAME)
  element = fill_field(driver, "pass", FACEBOOK_PASSWORD)
  element.submit
end

def team_data()
  data = []
  data << { aliases: ["lavenderlion", "lavenderlions"], name: "Lavender Lions" }
  data << { aliases: ["platinumpegasus", "platinumpegasuses", "platinumpegasi"], name: "Platinum Pegasus" }
  data << { aliases: ["redrobin", "redrobins"], name: "Red Robins" }
  data << { aliases: ["yellowyak", "yellowyaks"], name: "Yellow Yaks" }
  return data
end

def html(driver)
  puts driver.find_element(:tag_name, "html").attribute("outerHTML")
end

driver = load_driver()
link = "https://www.facebook.com/login/?next=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F1935183619921249%2F"
driver.get(link)
login(driver)
click(driver, :id, "checkpointSubmitButton")
