# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190612205004) do

  create_table "posts", force: :cascade do |t|
    t.string  "author",       default: ""
    t.string  "profile_link", default: ""
    t.string  "timestamp",    default: ""
    t.string  "message",      default: ""
    t.string  "team",         default: ""
    t.string  "challenges",   default: ""
    t.boolean "is_processed", default: false
    t.boolean "is_published", default: false
  end

  create_table "users", force: :cascade do |t|
    t.string  "name",        default: ""
    t.string  "email",       default: ""
    t.string  "profile_img", default: ""
    t.boolean "admin",       default: false
    t.string  "provider",    default: ""
    t.string  "uid",         default: ""
  end

end
