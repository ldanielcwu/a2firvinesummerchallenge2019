class AddPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string   :author,          :default => ""
      t.string   :profile_link,    :default => ""
      t.datetime :timestamp,       :default => nil
      t.string   :timestamp,       :default => ""
      t.string   :message,         :default => ""
      t.string   :team,            :default => ""
      t.string   :challenges,      :default => ""
      t.boolean  :is_processed,    :default => false
      t.boolean  :is_published,    :default => false
    end

    create_table :users do |t|
      t.string   :name,            :default => ""
      t.string   :email,           :default => ""
      t.string   :profile_img,     :default => ""
      t.boolean  :admin,           :default => false

      t.string   :provider,        :default => ""
      t.string   :uid,             :default => ""
    end

  end
end
